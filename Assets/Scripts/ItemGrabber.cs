using UnityEngine;

public class ItemGrabber : MonoBehaviour
{
    public ItemManager ItemManager;

    public Collector Collector;

    public void Start()
    {
        var items = FindObjectsOfType<Item>();
        ItemManager.Init(Collector, items);
    }
}