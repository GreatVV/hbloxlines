using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UnityEngine.Assertions.Must;
using UnityEngine.UI;

public class ItemManager : MonoBehaviour
{
    public Text LeftAmount;
    public Text TotalAmount;

    private int _totalAmount;
    private CompositeDisposable disposable;

    public void Init(Collector collector, IEnumerable<Item> items)
    {
        collector.MustNotBeNull();
        items.MustNotBeNull();
        items.Any().MustBeTrue();
        _totalAmount = collector.AmountOfItems;
        LeftAmount.text = TotalAmount.text = _totalAmount.ToString();
        if (disposable != null)
        {
            disposable.Dispose();
        }
        disposable = new CompositeDisposable
                     {
                         collector.ItemTaken.Subscribe
                             (
                              item =>
                              {
                                  LeftAmount.text = string.Format("Left: {0}", collector.AmountOfItemsLeft);
                              })
                     };
    }
    
    private void Start()
    {
        TotalAmount.MustNotBeNull();
        LeftAmount.MustNotBeNull();
    }
}