﻿using System.Collections.Generic;
using System.Linq;
using UniRx;
using Zenject;

public class GameViewForLevelFactory
{
    [Inject]
    private GameCubeViewFactory gameCubeViewFactory;

    public void CreateView(Level level)
    {
        if (level.Field.Cubes.Any())
        {
            CreateViews(level.Field.Cubes);
        }
        else
        {
            level.Field
                .CubesChanged
                .First()
                .Subscribe(
                _ =>{},
                () =>
                {
                     CreateViews(level.Field.Cubes);
                });
        }
    }

    private void CreateViews(IEnumerable<GameCube> cubes)
    {
        foreach (var gameCube in cubes)
        {
            gameCubeViewFactory.CreateView(gameCube);
        }
    }

}