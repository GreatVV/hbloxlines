﻿using System;

[Serializable]
public class LevelInfo
{
    public string Name;
    public CubeDesc[] cubes;
    public CubeDesc StartCubeDesc;
}