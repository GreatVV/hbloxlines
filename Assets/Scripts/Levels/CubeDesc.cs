﻿using System;
using System.Globalization;
using UnityEngine;

[Serializable]
public class CubeDesc
{
    public string Id;
    public SerializableVector3 Position;
    public SerializableVector3 Axis;
    public string[] Children;
    public CubePropertyDesc[] Properties;

    public static CubeDesc Create(GameCube cube)
    {
        return new CubeDesc()
               {
                   Position = new SerializableVector3(cube.transform.position),
                   Axis = new SerializableVector3(cube.Axis)
               };
    }
}

[Serializable]
public class SerializableVector3
{
    public float X;
    public float Y;
    public float Z;

    public Vector3 Vector3
    {
        get
        {
            return new Vector3(X, Y, Z);
        }
    }

    public SerializableVector3() { }
    public SerializableVector3(Vector3 vector)
    {
        float val;
        X = float.TryParse(vector.x.ToString(CultureInfo.InvariantCulture), out val) ? val : 0.0f;
        Y = float.TryParse(vector.y.ToString(CultureInfo.InvariantCulture), out val) ? val : 0.0f;
        Z = float.TryParse(vector.z.ToString(CultureInfo.InvariantCulture), out val) ? val : 0.0f;
    }
}