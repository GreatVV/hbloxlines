﻿using System;
using UnityEngine;

public class Level : MonoBehaviour
{
    public GameCube StartCube;
    public Field Field;
    public Player Player;
}