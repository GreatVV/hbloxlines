﻿using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Assertions.Must;
using Zenject;

public class CubeInteraction
{
    [Inject]
    private GameCubeViewFactory gameCubeViewFactory;

    public void Init(Controller controller)
    {
        controller.MustNotBeNull();
        controller.Field.MustNotBeNull();
        controller.Player.MustNotBeNull();

        controller.FieldUpdated.Subscribe
            (
             _ =>
             {
                 AnalyzeElementalCubes(controller.Field.Cubes.Where(x=>x.Elemental()));
                 AnalyzePortals(controller.Player);
             });
    }

    private static void AnalyzePortals(Player player)
    {
        var playerPortal = player.transform.parent.GetComponent<PortalCube>();
        if (playerPortal && playerPortal.CanTeleport)
        {
            player.transform.SetParent(playerPortal.targetCube.transform, false);
            player.transform.localPosition = playerPortal.targetCube.GetComponent<GameCubeAnimation>().PlayerRelative;
        }
    }

    private void AnalyzeElementalCubes(IEnumerable<GameCube> elementalCubes)
    {
        Debug.Log("Analyze elemental cubes");
        var markedToDestroy = new List<ElementalCube>();
        foreach (var elementalCube in elementalCubes)
        {
            var cubeType = elementalCube.Elemental().CubeType;
            var neighbours = elementalCube.GetNeighbours(1f);
            var neigbourElemental = neighbours
                .Where(x => x.Elemental())
                .Select(x => x.Elemental())
                .Where(
                       x => cubeType == ElementalCube.Type.Fire && x.CubeType == ElementalCube.Type.Water ||
                            cubeType == ElementalCube.Type.Air && x.CubeType == ElementalCube.Type.Earth ||
                            cubeType == ElementalCube.Type.Water && x.CubeType == ElementalCube.Type.Fire ||
                            cubeType == ElementalCube.Type.Earth && x.CubeType == ElementalCube.Type.Air);
        

            
            markedToDestroy = markedToDestroy.Concat(neigbourElemental).Distinct().ToList();
        }
        Debug.LogFormat("Marked to destroy: {0}", markedToDestroy.Count);
        foreach (var elementalCube in markedToDestroy)
        {
            var cube = elementalCube.GetComponent<GameCube>();
            Object.Destroy(elementalCube);
            cube.UpdateAsObservable().First().DelayFrame(1).Subscribe
                (
                 _ =>
                 {
                     gameCubeViewFactory.CreateView(cube);
                 });

        }


    }
}