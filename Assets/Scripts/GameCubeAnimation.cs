using UniRx;
using UnityEngine;

[RequireComponent(typeof(GameCube))]
public class GameCubeAnimation : MonoBehaviour
{
    private GameCube cube;
    private MeshRenderer renderer;

    public Vector3 PlayerRelative = new Vector3(0, 0.5f, 0f);

    public MeshRenderer Renderer
    {
        get
        {
            return renderer;
        }
        set
        {
            renderer = value;
            UpdateState();
        }
    }

    public void Awake()
    {
        cube = GetComponent<GameCube>();
        cube.Updated += OnCubeChanged;
    }

    private void UpdateState()
    {
        if (!renderer)
        {
            return;
        }

        if (renderer != null)
        {
            renderer.material.color = cube.IsSimpleCube ? Color.red : Color.blue;
        }

    }

    private void OnCubeChanged(GameCube gameCube)
    {
        UpdateState();
    }

    

    public BoolReactiveProperty Animated = new BoolReactiveProperty();

    public void ShowAccessable(bool state)
    {
        if (!renderer)
        {
            return;
        }

        if (state)
        {
            renderer.material.color = Color.green;
        }
        else
        {
            UpdateState();
        }
    }
}