using UniRx.Triggers;
using UnityEngine;
using UniRx;
using UnityEngine.Assertions.Must;

public class PlayerControlSwitcher : MonoBehaviour
{
    private PlayerControlState playerControlState;

    private CubeRotationState cubeRotationState;

    private Controller Controller;

    public void GoToCubeRotationMode()
    {
        Controller.ChangeStateTo(cubeRotationState);
    }

    public void GoToPlayerMoveMode()
    {
        Controller.ChangeStateTo(playerControlState);
    }

    public void Toggle()
    {
        if (Controller.State == cubeRotationState)
        {
            GoToPlayerMoveMode();
        }
        else
        {
            GoToCubeRotationMode();
        }
    }

    public void Init(Player player)
    {
        playerControlState = GetComponent<PlayerControlState>();
        cubeRotationState = GetComponent<CubeRotationState>();
        Controller = GetComponent<Controller>();

        player.MustNotBeNull();
        player.OnPointerDownAsObservable().Subscribe
            (
             _ =>
             {
                 if (Controller.State != playerControlState)
                 {
                     GoToPlayerMoveMode();
                 }
                 else
                 {
                     GoToCubeRotationMode();
                 }
             });
        GoToCubeRotationMode();
    }
}