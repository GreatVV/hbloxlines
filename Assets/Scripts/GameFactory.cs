using UniRx;
using UnityEngine;
using UnityEngine.Assertions.Must;
using Zenject;
using Object = UnityEngine.Object;

public class GameFactory
{
    [Inject]
    private ObserverFactory ObserverFactory;

    private Level lastPlayedLevel;
    private GameObject lastGame;

    private ISubject<Level> _gameStarted = new Subject<Level>();

    public IObservable<Level> GameStarted
    {
        get
        {
            return _gameStarted;
        }
    }

    public GameObject StartGame(Level prefab)
    {
        lastPlayedLevel = prefab;
        var level = InstantiateLevel(prefab);
        ObserverFactory.Create(level, level.transform);
        _gameStarted.OnNext(level);
        return level.gameObject;
    }

    private Level InstantiateLevel(Level prefab)
    {
        var go = new GameObject("Game");
        var level = Object.Instantiate(prefab);
        level.transform.SetParent(go.transform, false);
        if (lastGame)
        {
            RemoveLastGame();
        }
        lastGame = go;
        return level;
    }

    public void Restart()
    {
        lastPlayedLevel.MustNotBeNull();
        RemoveLastGame();
        StartGame(lastPlayedLevel);
    }

    public void RemoveLastGame()
    {
        lastGame.MustNotBeNull();
        Object.Destroy(lastGame.gameObject);
    }
}