using Zenject;

public class GameSetup : MonoInstaller
{    
    public Player PlayerPrefab;
    public LevelFactory LevelFactory;
    public GameCubeViewFactory GameCubeViewFactory;
    public UiConfigurator UiConfigurator;
    public override void InstallBindings()
    {
        Container.Bind<LevelFactory>().ToSingle<LevelFactory>();
        Container.Bind<GameFactory>().ToSingle<GameFactory>();
        Container.Bind<GameViewForLevelFactory>().ToSingle<GameViewForLevelFactory>();
        Container.Bind<GameCubeViewFactory>().ToInstance(GameCubeViewFactory);
        Container.BindGameObjectFactory<Player.Factory>(PlayerPrefab.gameObject);
        Container.Bind<UiConfigurator>().ToInstance(UiConfigurator);
        Container.Bind<ObserverFactory>().ToSingle<ObserverFactory>();
        Container.Bind<CubeInteraction>().ToSingle<CubeInteraction>();
    }
}