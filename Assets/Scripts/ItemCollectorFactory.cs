using UnityEngine;
using Zenject;

public class ItemCollectorFactory : MonoBehaviour, ILevelObserver
{
    [Inject]
    private ObserverFactory observerFactory;

    [Inject]
    private UiConfigurator uiConfigurator;

    void Start()
    {
        observerFactory.Register(this);
    }

    public void Create(Level level, Transform parent)
    {
        var collector = new GameObject("Collector", typeof (Collector)).GetComponent<Collector>();
        var items = level.GetComponentsInChildren<Item>();

        collector.Init(level.Player, items);
        collector.transform.SetParent(parent, false);

        uiConfigurator.Init(collector, items);
    }
}