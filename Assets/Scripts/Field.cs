﻿using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

public class Field : MonoBehaviour
{
    private List<GameCube> _cubes = new List<GameCube>();

    public List<GameCube> Cubes
    {
        get
        {
            return _cubes;
        }
        private set
        {
            _cubes = value;
            _changed.OnNext(Unit.Default);
        }
    }

    [SerializeField]
    private bool needGrabCubes;

    void Start()
    {
        if (needGrabCubes)
        {
            GrabCubes();
        }
    }

    public void GrabCubes()
    {
        Cubes = GetComponentsInChildren<GameCube>().ToList();
        for (int i = 0; i < Cubes.Count; i++)
        {
            Cubes[i].name = "Cube " + i;
        }
    }

    void OnDestroy()
    {
        _changed.OnCompleted();
    }

    private ISubject<Unit> _changed = new Subject<Unit>();

    public IObservable<Unit> CubesChanged
    {
        get
        {
            return _changed;
        }
    }
}