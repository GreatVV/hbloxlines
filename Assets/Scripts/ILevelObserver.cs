using UnityEngine;

public interface ILevelObserver
{
    void Create(Level level, Transform parent);
}