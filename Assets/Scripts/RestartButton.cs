using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class RestartButton : MonoBehaviour, IPointerClickHandler
{
    [Inject]
    private GameFactory gameFactory;

    public void OnPointerClick(PointerEventData eventData)
    {
        gameFactory.Restart();
    }
}