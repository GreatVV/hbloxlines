using UnityEngine;
using Zenject;

public class CubeViewFactory : MonoBehaviour,ILevelObserver
{
    [Inject]
    private GameViewForLevelFactory GameViewForLevelFactory;

    [Inject]
    private ObserverFactory observerFactory;

    void Start()
    {
        observerFactory.Register(this);
    }

    public void Create(Level level, Transform parent)
    {
        GameViewForLevelFactory.CreateView(level);
    }
}