using UnityEngine;

public class Item : MonoBehaviour
{
    public bool IsAnimation;

    public void SelfDestroy()
    {
        Destroy(gameObject, 0.3f);
    }
}