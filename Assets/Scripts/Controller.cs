using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class Controller : MonoBehaviour
{
    private ControllerState _currentState;

    [SerializeField]
    private Field _field;
    [SerializeField]
    private Player _player;

    public ControllerState FirstState;

    public ControllerState State
    {
        get
        {
            return _currentState;
        }
    }

    private ISubject<Unit> _fieldUpdated = new Subject<Unit>();

    public IObservable<Unit> FieldUpdated
    {
        get
        {
            return _fieldUpdated;
        }
    }

    public Player Player
    {
        get
        {
            return _player;
        }
    }

    public Field Field
    {
        get
        {
            return _field;
        }
    }

    public IObservable<GameCube> CubeClicked()
    {
        return _field.Cubes.Select(x => x.Clicked()).Merge();
    }

    public void Init(Player player, Field field)
    {
        player.MustNotBeNull();
        field.MustNotBeNull();
        FirstState.MustNotBeNull();

        _player = player;
        _field = field;
        ChangeStateTo(FirstState);
    }

    void OnDestroy()
    {
        _fieldUpdated.OnCompleted();
    }


    void Start()
    {
        if (_player && _field)
        {
            Init(_player, _field);
        }
    }

    private CompositeDisposable _compositeDisposable;

    public void ChangeStateTo(ControllerState newState)
    {
        newState.MustNotBeNull();

        if (_compositeDisposable!= null)
        {
            _compositeDisposable.Dispose();
        }

        if (_currentState != null)
        {
            _currentState.OnExit();
        }

        _currentState = newState;
        _currentState.field = _field;
        _currentState.player = _player;

        if (_currentState != null)
        {
            _currentState.OnEnter(CubeClicked());
            _compositeDisposable = new CompositeDisposable()
                                  {
                                      _currentState.FieldStateUpdated.Subscribe(_ => _fieldUpdated.OnNext(_))
                                  };
        }
    }
}