using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class LevelFactoryHelper
{
    public static GameCube GetCubeAt(IEnumerable<GameCube> cubes, Vector3 position)
    {
        return cubes.First(x => x.transform.position == position);
    }

    public static List<GameCube> ForceGrabChildren(GameCube cube)
    {
        return cube.GetComponentsInChildren<GameCube>().Except(new[] { cube }).ToList();
    }

    public static T Random<T>(this IEnumerable<T> items)
    {
        return items.ElementAt(UnityEngine.Random.Range(0, items.Count()));
    }
}