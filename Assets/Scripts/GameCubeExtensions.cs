using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class GameCubeExtensions
{
    public static List<GameCube> GetNeighbours(this GameCube cube, float radius)
    {
        var neightbours = Physics.OverlapSphere(cube.transform.position, radius);

        if (neightbours.Any())
        {
            return neightbours.Select(x => x.GetComponent<GameCube>()).Where(x => x).Except(new[] { cube }).ToList();
        }
        return new List<GameCube>();
    }

    public static ElementalCube Elemental(this GameCube cube)
    {
        return cube.GetComponent<ElementalCube>();
    }

    public static PortalCube Portal(this GameCube cube)
    {
        return cube.GetComponent<PortalCube>();
    }
    public static GameCube UpperNeigbour(this GameCube cube, float radius)
    {
        var neightbours = GetNeighbours(cube, radius);
        var upper = neightbours.FirstOrDefault(
                                          x => Mathf.Abs(x.transform.position.x - cube.transform.position.x) < ControlUtility.Epsilon &&
                                               Mathf.Abs(x.transform.position.z - cube.transform.position.z) < ControlUtility.Epsilon &&
                                               x.transform.position.y > cube.transform.position.y + ControlUtility.Epsilon);
        return upper;
    }

    
}