using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UniRx;

public class PlayerControlState : ControllerState
{
    public float AnimationTime = 0.1f;
    private List<GameCubeAnimation> cubesAnimations = new List<GameCubeAnimation>();
    private GameCube[] availableCubes;

    protected override void Exit()
    {
        ShowAccessable(false);
    }

    protected override void Enter()
    {
        ShowAccessable(true);
    }

    private void ShowAccessable(bool state)
    {
        if (state)
        {
            availableCubes = PathFinder.FindAvailableCubes(field.Cubes, player);
            cubesAnimations = availableCubes.Select(x => x.GetComponent<GameCubeAnimation>()).ToList();
            foreach (var gameCubeAnimation in cubesAnimations)
            {
                gameCubeAnimation.ShowAccessable(true);
            }
        }
        else
        {
            foreach (var gameCubeAnimation in cubesAnimations)
            {
                gameCubeAnimation.ShowAccessable(false);
            }
        }
    }

    protected override IDisposable ActionOnClickCube(IObservable<GameCube> clickedCubes)
    {
        return clickedCubes.Where(x => availableCubes.Contains(x)).Subscribe
            (
             _ =>
             {
                 PlayPlayerAnimation(_).Subscribe(x=> {},
                                                  () =>
                                                  {
                                                      _fieldStateUpdated.OnNext(Unit.Default);
                                                  });
             });
    }

    private IObservable<GameCube> PlayPlayerAnimation(GameCube targetCube)
    {
        if (player.IsAnimated)
        {
            return Observable.Empty<GameCube>();
        }

        if (!availableCubes.Contains(targetCube))
        {
            return Observable.Empty<GameCube>();
        }
        
        return Observable.Create<GameCube>(observable =>
        {
            player.IsAnimated = true;
            var path = PathFinder.PathToCube(availableCubes, player.Cube(), targetCube, player);
            
                var sequence = DOTween.Sequence();
                foreach (var gameCube in path)
                {
                    var cubeAnimation = gameCube.GetComponent<GameCubeAnimation>();
                    var tweener = player.transform.DOJump
                        (cubeAnimation.transform.position + cubeAnimation.PlayerRelative, 1, 1, AnimationTime);
                    tweener.Join
                        (player.transform.DOLookAt(gameCube.transform.position, AnimationTime, AxisConstraint.Y));
                    sequence.Append(tweener);
                }

                sequence.OnComplete
                    (
                     () =>
                     {
                         player.IsAnimated = false;
                         player.transform.SetParent(targetCube.transform, true);
                         observable.OnNext(targetCube);
                         observable.OnCompleted();
                         ShowAccessable(true);
                     });

                sequence.Play();
            
            
            ShowAccessable(false);
            return Disposable.Empty;
        });
    }
}