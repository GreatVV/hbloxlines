﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using DG.Tweening;
using UniRx;
using UniRx.Triggers;

public class GameCamera : MonoBehaviour
{

    public Transform target;
    public Camera camera;

    public float minZoom = -18;
    public float maxZoom = -5;
    public float velocity = 10f;

    public Vector3 axis = Vector3.up;

    void Start()
    {
        Zoom(minZoom, maxZoom, velocity);
        Rotate();
    }

    void Zoom(float min, float max, float zoomSpeed)
    {
        var targetZoom = gameObject.UpdateAsObservable()
            .Select(_ => Input.mouseScrollDelta.y)
            .Where(x => Mathf.Abs(x) > Mathf.Epsilon)
            .Select(x => new Vector3(camera.transform.localPosition.x, camera.transform.localPosition.y, Mathf.Clamp(camera.transform.localPosition.z + x*zoomSpeed, min, max)))
            .Distinct();
        
        Func<Vector3, IObservable<Vector3>> lerpToPosition = targetVector =>
        {
            return Observable.Create<Vector3>(observer =>
            {
                var tweener = camera.transform.DOLocalMove(targetVector, 0.2f);
                tweener.OnComplete(observer.OnCompleted);

                return Disposable.Create(() => {tweener.Kill(false);});
            });
        };

        
        var zoom = from target in targetZoom
            from pos in lerpToPosition(target)
            select target;

        zoom.Subscribe();
    }

    void Rotate()
    {
        var dragDelta = gameObject.UpdateAsObservable().Select(x => Input.mousePosition).Buffer(2).Select(buffer =>
        {
            var delta = buffer.Last() - buffer.First();
            return delta;
        }).Where(delta => delta.magnitude > Mathf.Epsilon);

        var buttonDown = gameObject.UpdateAsObservable().Where(x => Input.GetMouseButtonDown(0));
        var buttonUp = gameObject.UpdateAsObservable().Where(x => Input.GetMouseButtonUp(0));

        buttonDown.Subscribe(_ =>
        {
            dragDelta.TakeUntil(buttonUp).Subscribe(delta =>
            {
                //camera.transform.LookAt(target.position, axis);
                //camera.transform.RotateAround(target.position, Vector3.up, delta.x);
                target.localRotation = Quaternion.Euler(target.localRotation.eulerAngles + new Vector3(0, delta.x, 0));
            });
        });

    }
}