using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.Assertions.Must;
using Zenject;

public class CenterCameraOnField : MonoBehaviour
{
    [SerializeField]
    private Field field;

    [SerializeField]
    private GameObject target;

    [SerializeField]
    private float baseY;

    [Inject]
    public GameFactory GameFactory;

    public void Start()
    {
        if (field)
        {
            if (field.Cubes.Any())
            {
                Center(target.transform, field, baseY);
            }
            else
            {
                field.CubesChanged.First().Subscribe(_ =>
                {
                    Center(target.transform, field, baseY);
                });
            }
        }

        GameFactory.GameStarted.Subscribe
            (
             level =>
             {
                 field = level.Field;
                 if (field.Cubes.Any())
                 {
                     Center(target.transform, field, baseY);
                 }
                 else
                 {
                     field.CubesChanged.First().Subscribe(_ =>
                     {
                         Center(target.transform, field, baseY);
                     });
                 }
             });
    }

    public static void Center(Transform target, Field field, float baseY)
    {
        target.MustNotBeNull();
        field.MustNotBeNull();
        field.Cubes.Any().MustBeTrue("No cubes on field");
        var minX = field.Cubes.Min(x => x.transform.position.x);
        var maxX = field.Cubes.Max(x => x.transform.position.x);

        var minZ = field.Cubes.Min(x => x.transform.position.z);
        var maxZ = field.Cubes.Max(x => x.transform.position.z);

        var center = new Vector3(minX + (maxX - minX)/2f, baseY, minZ + (maxZ - minZ)/2f);
        target.transform.position = center;
    }
}