﻿public static class PlayerExtensions
{
    public static GameCube Cube(this Player player)
    {
        return player.transform.parent.GetComponent<GameCube>();
    }
}