using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class Collector : MonoBehaviour
{
    private ISubject<Unit> _allItemTaken = new Subject<Unit>();
    private List<Item> _items;
    private ISubject<Item> _itemTaken = new Subject<Item>();

    [SerializeField]
    private Player _player;

    public int AmountOfItems;
    public int AmountOfItemsLeft;

    public IObservable<Unit> AllItemTaken
    {
        get
        {
            return _allItemTaken;
        }
    }

    public IObservable<Item> ItemTaken
    {
        get
        {
            return _itemTaken;
        }
    }

    public void Init(Player player, IEnumerable<Item> items)
    {
        player.MustNotBeNull();
        items.MustNotBeNull();
        items.Any().MustBeTrue();
        _player = player;
        _items = items.ToList();
        AmountOfItems = _items.Count();
        CheckTake();
    }

    private IObservable<Item> ItemGrabbed()
    {
        return from item in _player.OnTriggerEnterAsObservable().Select(x => x.GetComponent<Item>()).Where(x => x)
               from animation in TakeAnimation(item)
               select item;
    }

    public void CheckTake()
    {
        ItemGrabbed().Subscribe
            (
             takenItem =>
             {
                 Debug.LogFormat("Take: {0}", takenItem);
                 _items.Remove(takenItem);
                 AmountOfItemsLeft = _items.Count;

                 _itemTaken.OnNext(takenItem);
                 if (AmountOfItemsLeft == 0)
                 {
                     _allItemTaken.OnNext(Unit.Default);
                 }
             });
    }

    private IObservable<Unit> TakeAnimation(Item item)
    {
        if (item.IsAnimation)
        {
            return Observable.Empty<Unit>();
        }

        return Observable.Create<Unit>
            (
             observable =>
             {
                 item.IsAnimation = true;
                 var sequence = DOTween.Sequence();
                 sequence.Append(item.transform.DOPunchScale(Vector3.one, 0.3f));
                 sequence.Append(item.transform.DOScale(Vector3.zero, 0.3f));

                 sequence.OnComplete
                     (
                      () =>
                      {
                          item.IsAnimation = false;
                          observable.OnNext(Unit.Default);
                          observable.OnCompleted();
                          item.SelfDestroy();
                      });

                 return Disposable.Empty;
             });
    }
}