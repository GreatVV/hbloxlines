public interface ICubeFactory
{
    void Create(GameCube gameCube, CubePropertyDesc cubePropertyDesc, Level level);

}