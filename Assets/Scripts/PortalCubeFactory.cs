using System.Linq;
using UnityEngine;
using Zenject;

class PortalCubeFactory : MonoBehaviour, ICubeFactory
{
    [Inject]
    private LevelFactory levelFactory;

    [SerializeField]
    private GameObject PortalPlatePrefab;

    void Start()
    {
        levelFactory.Register(this);
    }

    public void Create(GameCube gameCube, CubePropertyDesc cubePropertyDesc, Level level)
    {
        var portal = cubePropertyDesc as PortalCubePropertyDesc;
        if (portal != null)
        {
            var portalCube = gameCube.gameObject.AddComponent<PortalCube>();
            portalCube.targetCube = level.Field.Cubes.First(x=>x.name == portal.TargetCube);
            var portalPlayer = Instantiate(PortalPlatePrefab);
            portalPlayer.transform.SetParent(portalCube.transform, false);
            portalPlayer.transform.position = Vector3.up;
        }
    }
}