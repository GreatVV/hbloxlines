using UnityEngine;
using Zenject;

public class GameControlFactory : MonoBehaviour, ILevelObserver
{
    public Controller prefab;

    [Inject]
    private ObserverFactory observerFactory;

    [Inject]
    private CubeInteraction cubeInteraction;

    public void Start()
    {
        observerFactory.Register(this);
    }

    public void Create(Level level, Transform parent)
    {
        var controller = Instantiate(prefab);
        controller.transform.SetParent(parent, false);
        controller.Init(level.Player, level.Field);
        controller.GetComponent<PlayerControlSwitcher>().Init(level.Player);

        cubeInteraction.Init(controller);
    }
}