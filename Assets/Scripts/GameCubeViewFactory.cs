using UnityEngine;
using UnityEngine.Assertions.Must;

public class GameCubeViewFactory : MonoBehaviour
{
    [SerializeField]
    private GameObject[] ElementalCubePrefab;

    [SerializeField]
    private GameObject[] PortlaCubePrefab;

    [SerializeField]
    private GameObject[] SimpleCubePrefab;

    void Start()
    {
        ElementalCubePrefab.MustNotBeNull();
        PortlaCubePrefab.MustNotBeNull();
        SimpleCubePrefab.MustNotBeNull();
    }

    public void CreateView(GameCube cube)
    {
        cube.MustNotBeNull();
        RemoveViewInChildren(cube.transform);
        var go = Create(cube);
        go.AddComponent<CubeView>();
        go.transform.SetParent(cube.transform, false);
        cube.GetComponent<GameCubeAnimation>().Renderer = go.GetComponent<MeshRenderer>();
    }

    private static void RemoveViewInChildren(Transform transform)
    {
        foreach (Transform child in transform)
        {
            if (child.GetComponent<CubeView>())
            {
                Destroy(child.gameObject, 0.5f);
            }
        }
    }

    private GameObject Create(GameCube cube)
    {
        var elemental = cube.GetComponent<ElementalCube>();
        if (elemental)
        {
            return Instantiate(ElementalCubePrefab.Random());
        }

        var portal = cube.GetComponent<PortalCube>();
        if (portal)
        {
            return Instantiate(PortlaCubePrefab.Random());
        }

        return Instantiate(SimpleCubePrefab.Random());
    }
}