using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ControlUtility
{
    public const float Epsilon = 0.1f;

    public static bool HasCubesAt(IEnumerable<GameCube> cubes, IEnumerable<Vector3> worldPositions)
    {
        return worldPositions.Any(x => cubes.Any(cube => Vector3.Distance(cube.transform.position, x) < 0.01f));
    }

    public static List<Vector3> Rotate(GameCube cube, Vector3 angle)
    {
        var pivot = cube.transform.position;
        var children = cube.transform.GetComponentsInChildren<Transform>().Select(x => x.position);

        return children.Select(x => x.RotateAroundPivot(pivot, angle)).Except(new[] { pivot }).ToList();
    }

    public static List<Vector3> Rotate(GameCube cube, Quaternion angle)
    {
        var pivot = cube.transform.position;
        var children = cube.transform.GetComponentsInChildren<Transform>().Select(x => x.position);

        return children.Select(x => x.RotateAroundPivot(pivot, angle)).Except(new[] { pivot }).ToList();
    }

    public static bool HasPlayerInChildren(GameCube gameCube)
    {
        return gameCube.GetComponentInChildren<Player>();
    }

    public static bool SameFloor(GameCube gameCube, Player player)
    {
        return Math.Abs(player.transform.parent.position.y - gameCube.transform.position.y) < ControlUtility.Epsilon;
    }

    public static bool IsPlayerCloseEnough(GameCube gameCube, Player player)
    {
        return Vector3.Distance(gameCube.transform.position, player.transform.position) < player.MaxJump;
    }
}