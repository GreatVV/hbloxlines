using System;
using UniRx;
using UnityEngine;

[Serializable]
public abstract class ControllerState : MonoBehaviour
{
    private CompositeDisposable compositeDisposable;
    [NonSerialized]
    public Field field;
    [NonSerialized]
    public Player player;

    protected ISubject<Unit> _fieldStateUpdated = new Subject<Unit>();

    public void OnExit()
    {
        if (compositeDisposable != null)
        {
            compositeDisposable.Dispose();
        }
        Exit();
    }

    public IObservable<Unit> FieldStateUpdated
    {
        get
        {
            return _fieldStateUpdated;
        }
    }

    protected abstract void Exit();

    protected abstract void Enter();

    public void OnEnter(IObservable<GameCube> clickedCubes)
    {
        compositeDisposable = new CompositeDisposable
                              {
                                  ActionOnClickCube(clickedCubes)
                              };
        Enter();
    }

    protected abstract IDisposable ActionOnClickCube(IObservable<GameCube> clickedCubes);
}