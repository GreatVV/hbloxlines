﻿using UnityEngine;
using System.Collections;
using Zenject;

public class Player : MonoBehaviour
{
    public float MaxJump = 1.3f;
    public bool IsAnimated;

    public class Factory : GameObjectFactory<Player>
    {
        
    }
}