using UniRx;
using UnityEngine;
using Zenject;

public class PrefabGameStarter : MonoBehaviour
{
    [Inject]
    private GameFactory _gameFactory;

    [SerializeField]
    private Level _prefab;

    void Start()
    {
        if (_prefab)
        {
            Observable.Empty<Unit>().DelayFrame(1, FrameCountType.Update).Subscribe(_=> { }, () =>
            {
                _gameFactory.StartGame(_prefab);
            });
        }
    }
}