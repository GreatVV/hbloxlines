using System;
using System.Linq;
using DG.Tweening;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class CubeRotationState : ControllerState
{
    public float AnimationTime = 0.3f;

    protected override void Exit()
    {
        
    }

    protected override void Enter()
    {
        
    }

    protected override IDisposable ActionOnClickCube(IObservable<GameCube> clickedCubes)
    {
        var click = clickedCubes.Where(x => !x.IsSimpleCube && !ControlUtility.HasPlayerInChildren(x));

        return click.Subscribe(_ =>
                               {
                                   RotatingCubeClicked(_, AnimationTime).Subscribe(x=> {}, e=> {}, () => {_fieldStateUpdated.OnNext(Unit.Default);});
                               },
            () => { Debug.Log("CubeRotationState finished"); });
    }


    public static IObservable<Unit> RotatingCubeClicked(GameCube cube, float animationTime)
    {
        var cubeAnimation = cube.GetComponent<GameCubeAnimation>();
        if (cubeAnimation.Animated.Value)
        {
            //Debug.LogFormat("Can't rotate because animation is true on: {0}", cube.name);
            return Observable.Empty<Unit>();
        }

        return Observable.Create<Unit>
            (
             observer =>
             {

                 var relativeLocalRotation = (cube.Clockwise ? -1 : 1) * cube.Axis * 90;
                 var startRotation = cube.transform.localRotation.eulerAngles;
                 //var targetRotation = cube.transform.localRotation.eulerAngles + relativeLocalRotation;

                 cubeAnimation.Animated.Value = true;

                 var rotationToPosition =
                     LocalRotationAnimation(cube, relativeLocalRotation, animationTime, true).Subscribe
                         (
                          _ =>
                          {
                          },
                          () =>
                          {
                              cubeAnimation.Animated.Value = false;
                              observer.OnCompleted();
                          });

                 var rotateToStartPosition = LocalRotationAnimation(cube, startRotation, animationTime);

                 var returnAnimationSubscription =
                     cube.Children.Select(x => x.OnTriggerEnterAsObservable())
                         .Merge()
                         .First()
                         .Select(x => rotateToStartPosition)
                         .Switch()
                         .Subscribe
                         (
                          _ =>
                          {
                              Debug.Log("Collide with cube at " + _);
                              cube.InverseRotation();
                              rotationToPosition.Dispose();
                          },
                          () =>
                          {
                              cubeAnimation.Animated.Value = false;
                              observer.OnCompleted();
                          });

                 cubeAnimation.Animated.Where(x => !x).First().Subscribe
                     (
                      _ =>
                      {
                      },
                      () =>
                      {
                          rotationToPosition.Dispose();
                          returnAnimationSubscription.Dispose();
                      });

                 return Disposable.Empty;
             });
    }

    private static IObservable<Vector3> LocalRotationAnimation(GameCube cube, Vector3 targetRotation, float time, bool relative = false)
    {
        return Observable.Create<Vector3>(observable =>
        {
            var tweener = cube.transform.DOLocalRotate(targetRotation, time, relative ? RotateMode.LocalAxisAdd : RotateMode.Fast);
            observable.OnNext(cube.transform.localRotation.eulerAngles);
            //tweener.OnUpdate(() => { observable.OnNext(cube.transform.localRotation.eulerAngles); });
            //observable.OnNext(cube.transform.localRotation.eulerAngles);
            tweener.OnComplete(() =>
            {
                observable.OnCompleted();
            });

            return Disposable.Create(() =>{ tweener.Pause(); });
        });
    }

}