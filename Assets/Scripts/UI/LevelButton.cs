﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LevelButton : UIBehaviour
{
    private int _number;

    [SerializeField]
    private Text _numberText;

    public Level Prefab;

    public int Number
    {
        get
        {
            return _number;
        }
        set
        {
            _number = value;
            _numberText.text = _number.ToString();
        }
    }
}