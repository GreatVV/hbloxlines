﻿using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class StartGameUIHider : UIBehaviour
{
    [Inject]
    public GameFactory GameFactory;

    public GameObject[] HideOnStart;
    public GameObject[] ShowOnStart;
    [PostInject]
    void PostInject()
    {
        GameFactory.GameStarted.Subscribe(
                                          level =>
                                          {
                                              foreach (var go in HideOnStart)
                                              {
                                                  go.SetActive(false);
                                              }

                                              foreach (var go in ShowOnStart)
                                              {
                                                  go.SetActive(true);
                                              }
                                          }
                                          );
    }

}