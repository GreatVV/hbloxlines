﻿using UnityEngine;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine.Assertions.Must;
using Zenject;

public class LevelChoser : MonoBehaviour
{
    [SerializeField]
    private Level[] levelPrefabs;

    [SerializeField]
    private RectTransform parent;

    [SerializeField]
    private LevelButton levelButtonPrefab;

    [Inject]
    private GameFactory gameFactory;
   
    void OnDisable()
    {
        foreach (Transform childTransform in parent)
        {
            Destroy(childTransform.gameObject);
        }
    }

    void OnEnable()
    {
        CreateButtons();
    }

    private void CreateButtons()
    {
        parent.MustNotBeNull();
        parent.childCount.MustBeEqual(0);
        levelButtonPrefab.MustNotBeNull();
        levelPrefabs.MustNotBeNull();

        var levelButtons = levelPrefabs.Select
            (
             levelPrefab =>
             {
                 var lb = Instantiate(levelButtonPrefab);
                 lb.Prefab = levelPrefab;
                 lb.transform.SetParent(parent, false);
                 return lb;
             }).ToArray();

        
        levelButtons.Select(x => x.OnPointerClickAsObservable().Select(y=>x)).Merge().First().Subscribe
            (
             level =>
             {
                 gameFactory.StartGame(level.Prefab);
             });
             
        for (int i = 0; i < levelButtons.Count(); i++)
        {
            var button = levelButtons[i];
            button.Number = i + 1;
        }
    }
}