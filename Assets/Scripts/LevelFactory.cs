using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelFactory
{
    [SerializeField]
    private GameObject portalPlatePrefab;

    public Level CreateLevel(LevelInfo levelInfo, GameCube cubePrefab)
    {
        var level = new GameObject("Level "+levelInfo.Name, typeof(Level)).GetComponent<Level>();
        var gameCubePrefab = cubePrefab;
        var field = new GameObject("Field", typeof(Field)).GetComponent<Field>();
        level.Field = field;
        field.transform.SetParent(level.transform, true);
        var cubes = levelInfo.cubes.Select(
                                           cubeDesc =>
                                           {
                                               var cube = Object.Instantiate(gameCubePrefab);
                                               cube.name = cubeDesc.Id;
                                               cube.transform.position = cubeDesc.Position.Vector3;
                                               return cube;
                                           }).ToList();

        foreach (var cube in cubes.OrderByDescending(x=>x.Children == null ? 0 : x.Children.Count))
        {
            var cubeDesc = levelInfo.cubes.First(x => x.Position.Vector3 == cube.transform.position);
            cube.Axis = cubeDesc.Axis.Vector3;
            if (cubeDesc.Children.Any())
            {
                foreach (var childId in cubeDesc.Children)
                {
                    var child = cubes.First(x => x.name == childId);
                    if (child.transform.parent == null)
                    {
                        child.transform.SetParent(cube.transform, true);
                    }
                }
            }
        }

        foreach (var cube in cubes)
        {
            if (cube.transform.parent == null)
            {
                cube.transform.SetParent(field.transform, true);
            }
        }

        foreach (var cubeDesc in levelInfo.cubes)
        {
            if (cubeDesc.Properties.Any())
            {
                var cube = LevelFactoryHelper.GetCubeAt(cubes, cubeDesc.Position.Vector3);
                foreach (var cubePropertyDesc in cubeDesc.Properties)
                {
                    foreach (var cubeFactory in factories)
                    {
                        cubeFactory.Create(cube, cubePropertyDesc, level);
                    }
                }
            }
        }

        level.StartCube = LevelFactoryHelper.GetCubeAt(cubes, levelInfo.StartCubeDesc.Position.Vector3);
        level.Field.GrabCubes();
        foreach (var gameCube in cubes)
        {
            gameCube.Children = LevelFactoryHelper.ForceGrabChildren(gameCube);
            gameCube.FireUpdated();
        }
        return level;
    }

    public void Register(ICubeFactory cubeFactory)
    {
        if (!factories.Contains(cubeFactory))
        {
            factories.Add(cubeFactory);
        }
    }

    private List<ICubeFactory> factories = new List<ICubeFactory>();
    
}