using Zenject;

public class ControllerSetup : MonoInstaller
{
    public Controller Controller;
    public CubeRotationState CubeRotationState;
    public PlayerControlState PlayerControlState;

    public override void InstallBindings()
    {
        Container.Bind<Controller>().ToInstance(Controller);
        Container.Bind<CubeRotationState>().ToInstance(CubeRotationState);
        Container.Bind<PlayerControlState>().ToInstance(PlayerControlState);

        Controller.FirstState = CubeRotationState;
    }
}