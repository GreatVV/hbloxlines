using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class UiConfigurator : MonoBehaviour
{
    public ItemManager itemManager;

    public WinPopup WinPopup;
    public GameObject GameUI;

    public void Init(Collector collector, IEnumerable<Item> items)
    {
        itemManager.MustNotBeNull();
        WinPopup.MustNotBeNull();
        GameUI.MustNotBeNull();
        itemManager.Init(collector, items);
        collector.AllItemTaken.Subscribe
            (_ =>
             {
                 WinPopup.gameObject.SetActive(true);
                 GameUI.gameObject.SetActive(false);
             });
    }
}