using System.Collections.Generic;
using UnityEngine;

public class ObserverFactory
{
    private List<ILevelObserver> observers = new List<ILevelObserver>();

    public void Create(Level level, Transform parenTransform)
    {
        foreach (var levelObserver in observers)
        {
            levelObserver.Create(level, parenTransform);
        }
    }

    public void Register(ILevelObserver observer)
    {
        if (!observers.Contains(observer))
        {
            observers.Add(observer);
        }
    }
}