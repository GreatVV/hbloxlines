using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathFinder
{
    public static GameCube[] FindAvailableCubes(IEnumerable<GameCube> cubes, Player player)
    {
        var startTile = player.transform.parent.GetComponent<GameCube>();
        var allTilesOnSameLevel = cubes.Where
            (x => Mathf.Abs(x.transform.position.y - startTile.transform.position.y) < ControlUtility.Epsilon).Except(new[] {startTile}).ToArray();

        var availableCubes = Physics.OverlapSphere(player.transform.position, player.MaxJump)
                                    .Select(x => x.GetComponent<GameCube>())
                                    .Where(x => x && !x.GetComponent<ElementalCube>())
                                    .Intersect(allTilesOnSameLevel).ToList();

        var newCubes = new List<GameCube>(availableCubes);

        while (newCubes.Any())
        {
            var gameCube = newCubes.First();
            newCubes.Remove(gameCube);
            var animation = gameCube.GetComponent<GameCubeAnimation>();
            var overlapingCubes =
                Physics.OverlapSphere(gameCube.transform.position + animation.PlayerRelative, player.MaxJump)
                       .Select(x => x.GetComponent<GameCube>()).Where(x=>x && allTilesOnSameLevel.Contains(x) && !x.GetComponent<ElementalCube>());
            foreach (var overlapingCube in overlapingCubes)
            {
                if (!availableCubes.Contains(overlapingCube))
                {
                    availableCubes.Add(overlapingCube);
                    newCubes.Add(overlapingCube);
                }
            }
        }

        return availableCubes.Where(x=>!x.UpperNeigbour(player.MaxJump)).ToArray();
    }

    public static List<GameCube> PathToCube(IEnumerable<GameCube> cubes, GameCube startCube, GameCube targetCube, Player player)
    {
        var visited = new List<GameCube>();
        var possiblePaths = new List<List<GameCube>>()
                            {
                                new List<GameCube>()
                                {
                                    startCube
                                }
                            };

        while (possiblePaths.Any())
        {
            var p = possiblePaths.First();
            possiblePaths.Remove(p);

            var x = p.Last();
            if (visited.Contains(x))
            {
                continue;
            }

            if (x == targetCube)
            {
                p.Remove(startCube);
                return p;
            }
            visited.Add(x);

            var possible =
                Physics.OverlapSphere
                    (x.transform.position + x.GetComponent<GameCubeAnimation>().PlayerRelative, player.MaxJump)
                       .Select(_ => _.GetComponent<GameCube>())
                       .Intersect(cubes)
                       .Except(visited);

            possiblePaths.AddRange(possible.Select(gameCube => new List<GameCube>(p)
                                                               {
                                                                   gameCube
                                                               }));
        }

        Debug.LogError("No path");
        return null;
    }
}