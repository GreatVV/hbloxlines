using System;
using UnityEngine;

public class ElementalCube : MonoBehaviour
{
    public enum Type
    {
        Fire,
        Water,
        Air,
        Earth
    }

    public Type CubeType;

    void OnDrawGizmos()
    {
        switch (CubeType)
        {
            case Type.Fire:
                Gizmos.color = Color.red;
                break;
            case Type.Water:
                Gizmos.color = Color.blue;
                break;
            case Type.Air:
                Gizmos.color = Color.gray;
                break;
            case Type.Earth:
                Gizmos.color = new Color32(165, 42,42, 255);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        Gizmos.DrawWireCube(transform.position, new Vector3(1, 1, 1));
    }
}