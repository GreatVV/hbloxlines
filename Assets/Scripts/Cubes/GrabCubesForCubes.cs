using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(GameCube))]
public class GrabCubesForCubes : MonoBehaviour
{
    public bool NeedGrab;

    void Start()
    {
        if (NeedGrab)
        {
            var cube = GetComponent<GameCube>();
            cube.Children = LevelFactoryHelper.ForceGrabChildren(cube);
        }
    }
}