using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

[DisallowMultipleComponent]
public class GameCube : MonoBehaviour
{
    public Vector3 Axis = Vector3.up;
    public bool Clockwise;
    private List<GameCube> _children = new List<GameCube>();

    public bool IsSimpleCube
    {
        get
        {
            return !_children.Any();
        }
    }

    public List<GameCube> Children
    {
        get
        {
            return _children;
        }
        set
        {
            _children = value;
            FireUpdated();
        }
    }

    public event Action<GameCube> Updated;

    public override string ToString()
    {
        return name + " Pos:" + transform.position;
    }

    void Awake()
    {
        Axis.Normalize();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(0.2f, 0.2f, 0.2f, 0.5F);
        Gizmos.DrawWireCube(transform.position, Vector3.one);
    }
    public IObservable<GameCube> Clicked()
    {
        return this.OnPointerDownAsObservable().Select(_=>this);
    }

    public void InverseRotation()
    {
        Clockwise = !Clockwise;
    }

    public virtual void FireUpdated()
    {
        if (Updated != null)
        {
            Updated.Invoke(this);
        }
    }
}