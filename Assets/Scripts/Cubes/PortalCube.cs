using UnityEngine;

public class PortalCube : MonoBehaviour
{
    public GameCube targetCube;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawSphere(transform.position, 0.4f);
        if (targetCube)
        {
            Gizmos.DrawLine(transform.position, targetCube.transform.position);
        }
        else
        {
            Gizmos.DrawIcon(transform.position, "exclamationMark.png", true);
        }
    }

    public bool CanTeleport
    {
        get
        {
            var targetPoint = targetCube.transform.TransformPoint
                (targetCube.GetComponent<GameCubeAnimation>().PlayerRelative);
            var targetExitPoint = targetCube.transform.position + (targetCube.GetComponent<GameCubeAnimation>().PlayerRelative);
            var targetIsFree = (targetPoint - targetExitPoint).magnitude < ControlUtility.Epsilon;

            var thisPoint = transform.TransformPoint(GetComponent<GameCubeAnimation>().PlayerRelative);
            var thisExitPoint = transform.position + (GetComponent<GameCubeAnimation>().PlayerRelative);
            var enterIsFree = (thisPoint - thisExitPoint).magnitude < ControlUtility.Epsilon;

            return targetIsFree && enterIsFree;
        }
    }
}