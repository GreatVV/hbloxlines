using UnityEngine;
using Zenject;

class ElementalCubeFactory : MonoBehaviour, ICubeFactory
{
    [Inject]
    private LevelFactory levelFactory;

    void Start()
    {
        levelFactory.Register(this);
    }

    public void Create(GameCube gameCube, CubePropertyDesc cubePropertyDesc, Level level)
    {
        var elemental = cubePropertyDesc as ElementalCubePropertyDesc;
        if (elemental != null)
        {
            var elementalCube = gameCube.gameObject.AddComponent<ElementalCube>();
            elementalCube.CubeType = elemental.Type;
        }
    }
}