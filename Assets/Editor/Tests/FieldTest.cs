﻿using UnityEngine;
using System.Collections;
using System.Linq;
using NUnit.Framework;

[TestFixture]
public class FieldTest
{
    [Test]
    public void CheckTilesAtMultiplePositions()
    {
        var expectedPosition1 = new Vector3(1, 10, 3);
        var expectedPosition2 = new Vector3(1, 11, 3);

        var field = GetField();
        var firstCube = AddCubeAtPosition(expectedPosition1);
        var secondCube = AddCubeAtPosition(expectedPosition2, firstCube);
        
        firstCube.transform.SetParent(field.transform, true);
        field.GrabCubes();

        var hasCubes = ControlUtility.HasCubesAt
            (
            field.Cubes,
             new[]
             {
                 expectedPosition1,
                 expectedPosition2
             });

        Assert.IsTrue(hasCubes);

        var inCorrectLocation = new Vector3(0, 1, 0);
        var noCubesAt = ControlUtility.HasCubesAt
            (
                field.Cubes,
             new[]
             {
                 inCorrectLocation
             });

        Assert.IsFalse(noCubesAt);
    }

    private GameCube AddCubeAtPosition(Vector3 position, GameCube parent = null)
    {
        var cube = new GameObject("Cube", typeof (GameCube)).GetComponent<GameCube>();
        cube.transform.position = position;
        if (parent)
        {
            cube.transform.SetParent(parent.transform, true);
        }
        return cube;
    }
    [Test]
    public void SimpleTwoCubesRotateTest()
    {
        var cube1 = AddCubeAtPosition(Vector3.zero);
        var cube2 = AddCubeAtPosition(Vector3.up, cube1);

        var targetPosition = new Vector3(0, 0, 1);
        var angle = new Vector3(90, 0, 0);

        var newPositions = ControlUtility.Rotate(cube1, angle);
        Assert.IsNotNull(newPositions);
        Assert.AreEqual(1, newPositions.Count);
        
        Assert.IsTrue(Vector3.Distance(targetPosition, newPositions.Last())< 0.01f);
    }

    [Test]
    public void SimpleThreeCubesRotateTest()
    {
        var cube1 = AddCubeAtPosition(Vector3.zero);
        var cube2 = AddCubeAtPosition(Vector3.up, cube1);
        var cube3 = AddCubeAtPosition(new Vector3(0, 1, 1), cube2);

        var targetPositionCube2 = new Vector3(0, 0, 1);
        var targetPositionCube3 = new Vector3(0, -1, 1);
        var angle = new Vector3(90, 0, 0);

        var newPositions = ControlUtility.Rotate(cube1, angle);
        Assert.IsNotNull(newPositions);
        Assert.AreEqual(2, newPositions.Count);

        Assert.IsTrue(Vector3.Distance(targetPositionCube2, newPositions[0]) < 0.01f);
        Assert.IsTrue(Vector3.Distance(targetPositionCube3, newPositions[1]) < 0.01f);
    }

    private Field GetField()
    {
        return new GameObject("Field", typeof(Field)).GetComponent<Field>();
    }
}
