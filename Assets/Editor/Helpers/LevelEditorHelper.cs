﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine.Assertions.Must;
using Object = UnityEngine.Object;

public static class LevelEditorHelper
{

    [MenuItem("Level Editor/Change Box Collider to Sphere Collider")]
    public static void ChangeBoxColliderToSphereCollider()
    {
        var selection = Selection.objects;
        if (selection == null ||
            !selection.Any())
        {
            return;
        }

        var go = selection.First() as GameObject;
        if (go != null)
        {
            var children = go.GetComponentsInChildren<BoxCollider>();
            foreach (var boxCollider in children)
            {
                var gameObject = boxCollider.gameObject;
                var size = boxCollider.size;
                Object.DestroyImmediate(boxCollider);
                var sphereCollider = gameObject.AddComponent<SphereCollider>();
                sphereCollider.isTrigger = true;
                sphereCollider.radius = size.x * 0.4f;
            }
        }
    }

    [MenuItem("Level Editor/Create Level Info from Cubes on screen")]
    public static void CreateLevelFromCubes()
    {
        var path = Path.Combine(Application.dataPath, "Resources/Levels.xml");

        var levelManager = Load<LevelManager>(path);
        if (levelManager == null)
        {
            Debug.LogError("No level manager");
            levelManager = new LevelManager();
        }
        

        var cubeParsing = new CubeParsing();

        var getCubes = Object.FindObjectsOfType<GameCube>();
        foreach (var gameCube in getCubes)
        {
            gameCube.Children = LevelFactoryHelper.ForceGrabChildren(gameCube);
        }
        Debug.LogFormat("Grab {0} cubes", getCubes.Count());

        var descs = new Dictionary<Vector3, CubeDesc>();
        int id = 1;
        foreach (var gameCube in getCubes)
        {
            var position = gameCube.transform.position.Snap();
            var newCubeDesc = CubeDesc.Create(gameCube);
            newCubeDesc.Id = (id++).ToString();
            gameCube.name = newCubeDesc.Id;
            descs[position] = newCubeDesc;

        }

        foreach (var gameCube in getCubes)
        {
            var position = gameCube.transform.position.Snap();
            descs[position].Children = gameCube.Children.Select(x => descs[x.transform.position].Id).ToArray();
            descs[position].Properties = cubeParsing.GetProperties(gameCube);
        }

        var levelInfo = new LevelInfo
                        {
                            cubes = descs.Values.ToArray(),
                            StartCubeDesc = descs[PositionOfCubeWithPlayer(getCubes)]
                        };

        levelManager.levels.Add(levelInfo);

        Save(path, levelManager);
        Debug.Log("Complete");
        AssetDatabase.Refresh();
        AssetDatabase.SaveAssets();
    }


    private static void Save<T>(string path, T target)
    {
        Debug.LogFormat("Try to save to {0}", path);
        var serializer = new XmlSerializer(typeof(T));
        var stream = new FileStream(path, FileMode.Create);
        serializer.Serialize(stream, target);
        stream.Close();
    }

    private static T Load<T>(string path) where T:class
    {
        if (!File.Exists(path))
        {
            return null;
        }

        var serializer = new XmlSerializer(typeof(T));
        var stream = new FileStream(path, FileMode.Open);
        var container = serializer.Deserialize(stream) as T;
        stream.Close();

        return container;
    }

    private static Vector3 PositionOfCubeWithPlayer(GameCube[] cubes)
    {
        cubes.MustNotBeNull();
        cubes.Length.MustNotBeEqual(0);
        foreach (var gameCube in cubes)
        {
            foreach (Transform child in gameCube.transform)
            {
                if (child.GetComponent<Player>())
                {
                    return child.transform.position.Snap();
                }
            }
        }
        Debug.LogWarning("Can't find player! Add player to cube parent");
        return cubes.First().transform.position.Snap();
    }


    public static Vector3 Snap(this Vector3 vector)
    {
        return new Vector3(
             Mathf.Round(vector.x),
             Mathf.Round(vector.y),
             Mathf.Round(vector.z)
            );
    }

    [MenuItem("Utility/Create level manager")]
    public static void CreateLevelManager()
    {
      //  CreateAssetFile<LevelManager>();
    }

    public static void CreateAssetFile<T>() where T : ScriptableObject, new()
    {

        var path = string.Format("Assets/Resources/{0}.asset", typeof(T).Name);
        if (Selection.activeObject)
        {
            string selectionPath = AssetDatabase.GetAssetPath(Selection.activeObject); // relative path
            if (Directory.Exists(selectionPath))
            {
                path = Path.Combine(selectionPath, string.Format("{0}.asset", typeof(T).Name));
            }
        }

        var asset = ScriptableObject.CreateInstance<T>();
        AssetDatabase.CreateAsset(asset, path);
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}

