public class PortalCubeParser : ICubeParser
{
    public CubePropertyDesc Parse(GameCube gameCube)
    {
        var portalCube = gameCube.gameObject.GetComponent<PortalCube>();
        if (portalCube)
        {
            return new PortalCubePropertyDesc()
                   {
                       TargetCube = portalCube.name
                   };
        }
        return null;
    }
}