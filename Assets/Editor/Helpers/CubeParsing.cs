using System.Collections.Generic;
using System.Linq;

public class CubeParsing
{
    public List<ICubeParser> parsers = new List<ICubeParser>();

    public void Register(ICubeParser parser)
    {
        if (!parsers.Contains(parser))
        {
            parsers.Add(parser);
        }
    }

    public CubeParsing()
    {
        Register(new ElementalCubeParser());
        Register(new PortalCubeParser());
        Register(new ItemCubeParser());
    }

    public CubePropertyDesc[] GetProperties(GameCube cube)
    {
        return parsers.Select(cubeParser => cubeParser.Parse(cube)).Where(property => property != null).ToArray();
    }
}