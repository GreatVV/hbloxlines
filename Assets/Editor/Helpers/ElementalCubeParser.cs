public class ElementalCubeParser : ICubeParser
{
    public CubePropertyDesc Parse(GameCube gameCube)
    {
        var elementalCube = gameCube.gameObject.GetComponent<ElementalCube>();
        if (elementalCube)
        {
            return new ElementalCubePropertyDesc()
                   {
                       Type = elementalCube.CubeType
                   };
        }
        return null;
    }
}