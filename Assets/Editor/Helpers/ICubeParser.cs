public interface ICubeParser
{
    CubePropertyDesc Parse(GameCube gameCube);
}

