using System.Linq;
using UnityEngine;

public class ItemCubeParser : ICubeParser
{
    public CubePropertyDesc Parse(GameCube gameCube)
    {
        return gameCube.transform.Cast<Transform>().Any(child => child.GetComponent<Item>()) ? new ItemCubePropertyDesc() : null;
    }
}