//
//  LanguageManager.cs
//
//
// Written by Niklas Borglund and Jakob Hillerström
//

namespace SmartLocalization
{
using UnityEngine;
using System.Collections.Generic;
using System;

	/// <summary>
/// Change language event handler.
/// </summary>
public delegate void ChangeLanguageEventHandler(LanguageManager thisLanguageManager);

/// <summary>
/// The language worker class for runtime language handling
/// </summary>
public class LanguageManager : MonoBehaviour 
#if UNITY_4_6  || UNITY_5_0
	, ISerializationCallbackReceiver
#endif
{
    #region Singleton
    private static LanguageManager instance = null;

	/// <summary>
	/// Returns an instance of the language manager. Creates a new one if no one exist. 
	/// If you don't want to create a new one, check the LanguageManager.HasInstance variable first.
	/// </summary>
    public static LanguageManager Instance
    {
        get
        {
#if UNITY_EDITOR
			if(instance == null)
				instance = FindObjectOfType<LanguageManager>();
#endif
            if (instance == null)
            {
				if(!IsQuitting)
				{
					GameObject go = new GameObject();
					instance = go.AddComponent<LanguageManager>();
					go.name = "LanguageManager";

					if(!DidSetDontDestroyOnLoad && DontDestroyOnLoadToggle)
					{
						GameObject.DontDestroyOnLoad(instance);
						DidSetDontDestroyOnLoad = true;
					}
				}
            }

            return instance;
        }
    }

	/// <summary>
	/// Sets DontDestroyOnLoad flag on the LanguageManager object. 
	/// </summary>
	public static void SetDontDestroyOnLoad()
	{
		DontDestroyOnLoadToggle = true;

		if(instance != null && !DidSetDontDestroyOnLoad)
		{
			GameObject.DontDestroyOnLoad(instance);
			DidSetDontDestroyOnLoad = true;
		}
	}

	/// <summary>
	/// Returns if there is an active instance of the language manager in the game.
	/// </summary>
	public static bool HasInstance
	{
		get
		{
			return (instance != null);
		}
	}

	static bool IsQuitting = false;
	static bool DontDestroyOnLoadToggle = false;
	static bool DidSetDontDestroyOnLoad = false;

    #endregion

#if UNITY_4_6 || UNITY_5_0
#region Editor Serialization

	[SerializeField, HideInInspector]List<string> serializedKeys = null;
	[SerializeField, HideInInspector]List<LocalizedObject> serializedValues = null;

	public void OnAfterDeserialize()
	{
		if(serializedKeys == null)
		{
			return;
		}

		languageDatabase = new SortedDictionary<string,LocalizedObject>();

		for(int i = 0; i < serializedKeys.Count; ++i)
		{
			languageDatabase.Add(serializedKeys[i], serializedValues[i]);
		}

		serializedKeys.Clear();
		serializedValues.Clear();
	}

	public void OnBeforeSerialize()
	{
		if(serializedKeys == null)
		{
			serializedKeys = new List<string>();
		}
		if(serializedValues == null)
		{
			serializedValues = new List<LocalizedObject>();
		}

		serializedKeys.Clear();
		serializedValues.Clear();

		if(languageDatabase == null)
		{
			return;
		}

		foreach(var pair in languageDatabase)
		{
			serializedKeys.Add(pair.Key);
			serializedValues.Add(pair.Value);
		}
	}

#endregion
#endif

#region Members
	/// <summary>
	/// Occurs when a new language is loaded and initialized
	/// create a delegate method(ChangeLanguage(LanguageManager languageManager)) and subscribe
	/// </summary>
	public ChangeLanguageEventHandler OnChangeLanguage;
	
	/// <summary>
	/// The default language
	/// </summary>
	public string defaultLanguage = "en";
	
	/// <summary>
	/// The loaded language.
	/// </summary>
	string loadedLanguage = "en";

#if UNITY_4_6 || UNITY_5_0
	[SerializeField]
#endif
	SmartCultureInfo currentlyLoadedCulture = null;
#if UNITY_4_6 || UNITY_5_0
	[SerializeField]
#endif
	SmartCultureInfoCollection	availableLanguages = null;
	SortedDictionary<string, LocalizedObject> languageDatabase = null;

	bool verboseLogging = false;

#endregion

#region Properties
	
	/// <summary>
	/// The loaded language values of the currently loaded language
	/// </summary>
	public SortedDictionary<string, LocalizedObject> LanguageDatabase
	{
		get
		{
			return languageDatabase;
		}
	}
	
	/// <summary>
	/// Gets all the language values in raw text. NOTE: Potentially performance expensive. Use with caution.
	/// </summary>
	public Dictionary<string, string> RawTextDatabase
	{
		get
		{
			if(languageDatabase == null)
			{
				return null;
			}

			Dictionary<string, string> rawTextDB = new Dictionary<string, string>();
			foreach(KeyValuePair<string, LocalizedObject> pair in languageDatabase)
			{
				rawTextDB.Add(pair.Key, pair.Value.TextValue);
			}
			return rawTextDB;
		}
	}

	/// <summary>
	/// The number of supported languages in this project
	/// </summary>
	public int NumberOfSupportedLanguages
	{
		get
		{
			if(availableLanguages == null)
			{
				return 0;
			}

			return availableLanguages.cultureInfos.Count;
		}
	}

	/// <summary>
	/// The language code of the currently loaded language
	/// </summary>
	public string LoadedLanguage
	{
		get
		{
			return loadedLanguage;
		}
	}
	
	/// <summary>
	/// Gets the currently loaded SmartCultureInfo.
	/// </summary>
	/// <value>The currently loaded culture.</value>
	public SmartCultureInfo CurrentlyLoadedCulture
	{
		get
		{
			return currentlyLoadedCulture;
		}
	}

	/// <summary>
	/// Set this to true if you want extensive error logging
	/// </summary>
	public bool VerboseLogging
	{
		get
		{
			return verboseLogging;
		}
		set
		{
			verboseLogging = value;
		}
	}

#endregion

#region Mono
	void Awake ()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if(instance != this)
		{
			if(VerboseLogging)
			{
				Debug.LogError("Found duplicate LanguageManagers! Removing one of them");
			}

			Destroy(this);
			return;
		}
		
		if(LoadAvailableLanguages())
		{
			if(VerboseLogging)
			{
				Debug.Log ("LanguageManager.cs: Waking up");
			}	

			if(availableLanguages.cultureInfos.Count > 0)
			{
				SmartCultureInfo defaultLanguageCulture = availableLanguages.cultureInfos.Find(info => info.languageCode == defaultLanguage);
				
				if(defaultLanguageCulture != null)
				{
					ChangeLanguage(defaultLanguageCulture);
				}
				else
				{		
					//otherwise - load the first language in the list
					ChangeLanguage(availableLanguages.cultureInfos[0]);
					defaultLanguage = availableLanguages.cultureInfos[0].languageCode;
				}
			}
			else
			{
				Debug.LogError("LanguageManager.cs: No language is available! Use Window->Smart Localization tool to create a language");	
			}
		}
		else
		{
			Debug.LogError("LanguageManager.cs: No localization workspace is created! Use Window->Smart Localization tool to create one");
		}
	}

	void OnDestroy()
	{
		//Clear the event handler
		OnChangeLanguage = null;
	}

	void OnApplicationQuit()
	{
		IsQuitting = true;
	}

#endregion

#region Initialization
	bool LoadAvailableLanguages()
	{
		TextAsset availableLanguagesXML = Resources.Load(LanguageRuntimeData.AvailableCulturesFilePath()) as TextAsset;

		if(availableLanguagesXML == null)
		{
			Debug.LogError("Could not load available languages! No such file!");
			return false;
		}

		availableLanguages = SmartCultureInfoCollection.Deserialize(availableLanguagesXML);

		return true;
	}
#endregion

#region Load Language
		
	/// <summary>
	/// Change the language into a specified culture
	/// </summary>
	/// <param name="cultureInfo">The culture to change to</param>
	public void ChangeLanguage(SmartCultureInfo cultureInfo)
	{
		ChangeLanguage(cultureInfo.languageCode);
	}
	
	/// <summary>
	/// Change the language into a specified culture
	/// </summary>
	/// <param name="languageCode">The culture to change to</param>
	public void ChangeLanguage(string languageCode)
	{
		TextAsset languageData = Resources.Load(LanguageRuntimeData.LanguageFilePath(languageCode)) as TextAsset;
		
		if(languageData == null)
		{
			Debug.LogError("Failed to load language: " + languageCode);
			return;
		}
		
		LoadLanguage(languageData.text, languageCode);
		
		if(OnChangeLanguage != null)
		{
			OnChangeLanguage(this);	
		}
	}
		
	/// <summary>
	/// Tries to load a language with the specified data. Loading data from outside the project currently only supports text unless
	/// the asset is apparent(and correctly named in the right folder) in the workspace.
	/// </summary>
	/// <param name="languageDataInResX">The language data to load in the .resx format</param>
	/// <param name="languageCode">The ISO-639 code of the language to load</param>
	public void ChangeLanguageWithData(string languageDataInResX, string languageCode)
	{
		if(LoadLanguage(languageDataInResX, languageCode))
		{
			if(OnChangeLanguage != null){
				OnChangeLanguage(this);	
			}
		}
	}

	/// <summary>
	/// Appends the current language with text data. The method does not support appending localized assets.
	/// If duplicates are found, the old value <b>will be overwritten</b> with the new one.
	/// </summary>
	/// <param name="languageDataInResX">The language data to append in the .resx format</param>
	/// <returns>Returns if the operation was a success</returns>
	public bool AppendLanguageWithTextData(string languageDataInResX)
	{
		if(string.IsNullOrEmpty(languageDataInResX))
		{
			Debug.LogError("Failed to append data to the currently loaded language. Data was null or empty");
			return false;
		}

		if(languageDatabase == null)
		{
			Debug.LogError("Failed to append data to the currently loaded language. There is no language loaded.");
			return false;
		}

		var languageDatabaseToAppend = LanguageParser.LoadLanguage(languageDataInResX);

		foreach(var pair in languageDatabaseToAppend)
		{
			if(pair.Value.ObjectType != LocalizedObjectType.STRING)
			{
				continue;
			}

			if(languageDatabase.ContainsKey(pair.Key))
			{
				languageDatabase[pair.Key] = pair.Value;
			}
			else
			{
				languageDatabase.Add(pair.Key, pair.Value);
			}
		}

		if(verboseLogging)
		{
			Debug.Log("Appended or updated language:" + currentlyLoadedCulture.englishName + " with " + languageDatabaseToAppend.Count + " values");
		}

		return true;
	}

	bool LoadLanguage(string languageData, string languageCode)
	{	
		if(string.IsNullOrEmpty(languageData))
		{
			Debug.LogError("Failed to load language with ISO-639 code. Data was null or empty");
			return false;
		}
		
		if(languageDatabase != null)
		{
			languageDatabase.Clear();
			languageDatabase = null;
		}
		
		languageDatabase = LanguageParser.LoadLanguage(languageData);
		loadedLanguage = languageCode;
		currentlyLoadedCulture = GetCultureInfo(loadedLanguage);
		return true;
	}
		
#endregion
		

#region Lookups
	
	/// <summary>
	/// Checks if the language is supported by this application
	/// languageName = strings like "en", "es", "sv"
	/// </summary>
	public bool IsLanguageSupported(string languageCode)
	{
		if(availableLanguages == null)
		{
			Debug.LogError("LanguageManager is not initialized properly!");
			return false;	
		}
		
		return availableLanguages.cultureInfos.Find(info => info.languageCode == languageCode) != null;
	}

	/// <summary>
	/// Checks if the language is supported with the english name. i.e. "English" "French" etc.
	/// </summary>
	/// <param name="englishName">The english name of the language</param>
	/// <returns>If the language is supported</returns>
	public bool IsLanguageSupportedEnglishName(string englishName)
	{
		if(availableLanguages == null)
		{
			Debug.LogError("LanguageManager is not initialized properly!");
			return false;	
		}
		
		return availableLanguages.cultureInfos.Find(info => info.englishName.ToLower() == englishName.ToLower()) != null;
	}

	/// <summary>
	/// Checks if a language is supported with a culture info class
	/// </summary>
	/// <param name="cultureInfo">The culture info to check</param>
	/// <returns>If the language is supported</returns>
	public bool IsLanguageSupported(SmartCultureInfo cultureInfo)
	{
		return IsLanguageSupported(cultureInfo.languageCode);
	}
	
	/// <summary>
	/// Gets the culture info from the available languages list. If the language is not supported it will return null
	/// </summary>
	public SmartCultureInfo GetCultureInfo(string languageCode)
	{
		return availableLanguages.cultureInfos.Find(info => info.languageCode == languageCode);
	}

	/// <summary>
	/// Gets the name of the system language in an english name. 
	/// If its SystemLanguage.Unknown, a string with the value "Unknown" will be returned.
	/// </summary>
	public string GetSystemLanguageEnglishName()
	{
		return ApplicationExtensions.GetSystemLanguage();
	}

	/// <summary>
	/// Gets the smart culture info of the system language if it is supported. otherwise it will return null
	/// </summary>
	public SmartCultureInfo GetSupportedSystemLanguage()
	{
		if(availableLanguages == null)
		{
			return null;
		}

		string englishName = GetSystemLanguageEnglishName();

		return availableLanguages.cultureInfos.Find(info => info.englishName.ToLower() == englishName.ToLower());
	}
	
	/// <summary>
	/// Gets the language code of the system language if it is supported. otherwise it will return an empty string.
	/// </summary>
	public string GetSupportedSystemLanguageCode()
	{
		SmartCultureInfo systemLanguageCultureInfo = GetSupportedSystemLanguage();

		if(systemLanguageCultureInfo == null)
		{
			return string.Empty;
		}
		else
		{
			return systemLanguageCultureInfo.languageCode;
		}
	}

	/// <summary> Returns a list of all the supported languages in the project </summary>
	public List<SmartCultureInfo> GetSupportedLanguages()
	{
		if(availableLanguages == null)
		{
			Debug.LogError("LanguageManager is not initialized properly!");
			return null;	
		}

		return availableLanguages.cultureInfos;
	}
	
	/// <summary>
	/// Returns a list of keys within a specified category.
	/// Example: If you have keys named My.Key, the category would be "My."
	/// </summary>
	/// <returns>A list of keys that starts with the current category key</returns>
	/// <param name="category">If you have keys named My.Key, the category would be "My."</param>
	public List<string> GetKeysWithinCategory(string category)
	{
		var categoryList = new List<string>();
		if(string.IsNullOrEmpty(category) || languageDatabase == null)
		{
			return categoryList;
		}
		
		foreach(var pair in languageDatabase)
		{
			if(pair.Key.StartsWith(category)){
				categoryList.Add(pair.Key);
			}
		}
		
		return categoryList;
	}
	
#endregion


#region Get Language Values
	/// <summary>
	/// Returns a text value in the current language for the key. Returns null if nothing is found.
	/// </summary>
    public string GetTextValue(string key)
    {
		LocalizedObject localizedObject = GetLocalizedObject(key);
      
		if(localizedObject != null)
		{
			return localizedObject.TextValue;	
		}

		if(verboseLogging)
		{
			Debug.LogError("LanguageManager.cs: Invalid Key:" + key + "for language: " + loadedLanguage);
		}

        return null;
    }
	
	/// <summary>
	/// Returns a text value in the current language for the key with plural forms. Returns null if nothing is found.
	/// </summary>
	/// <returns>The text value.</returns>
	/// <param name="key">Key.</param>
	/// <param name="count">Plural form.</param>
	public string GetTextValue(string key, int count)
	{
		return GetTextValue(key + "_" + PluralForms.Languages[loadedLanguage](count).ToString());
	}
	
	/// <summary>
	/// Returns a text asset in the current language for the key with plural forms. Returns null if nothing is found.
	/// </summary>
	/// <returns>The text asset.</returns>
	/// <param name="key">Key.</param>
	/// <param name="count">Plural form.</param>
	public TextAsset GetTextAsset(string key, int count)
	{
		return GetTextAsset(key + "_" + PluralForms.Languages[loadedLanguage](count).ToString());
	}
	
	/// <summary>
	/// Returns an audio clip in the current language for the key with plural forms. Returns null if nothing is found.
	/// </summary>
	/// <returns>The audio clip.</returns>
	/// <param name="key">Key.</param>
	/// <param name="count">Plural form.</param>
	public AudioClip GetAudioClip(string key, int count)
	{
		return GetAudioClip(key + "_" + PluralForms.Languages[loadedLanguage](count).ToString());
	}
	
	/// <summary>
	/// Returns a prefab in the current language for the key with plural forms. Returns null if nothing is found.
	/// </summary>
	/// <returns>The prefab.</returns>
	/// <param name="key">Key.</param>
	/// <param name="count">Plural form.</param>
	public GameObject GetPrefab(string key, int count)
	{
		return GetPrefab(key + "_" + PluralForms.Languages[loadedLanguage](count).ToString());
	}
	
	/// <summary>
	/// Returns a texture in the current language for the key with plural forms. Returns null if nothing is found.
	/// </summary>
	/// <returns>The texture.</returns>
	/// <param name="key">Key.</param>
	/// <param name="count">Plural form.</param>
	public Texture GetTexture(string key, int count)
	{
		return GetTexture(key + "_" + PluralForms.Languages[loadedLanguage](count).ToString());
	}
	
	/// <summary>
	/// Returns a text value in the current language for the key with a custom plural form. Returns null if nothing is found.
	/// </summary>
	/// <returns>The text value.</returns>
	/// <param name="key">Key.</param>
	/// <param name="count">Count.</param>
	/// <param name="del">Del.</param>
	public string GetTextValue(string key, int count, Func<int, int> pluralForm)
	{
		return GetTextValue(key + "_" + pluralForm(count).ToString());
	}
	
	/// <summary>
	/// Returns a text asset in the current language for the key with a custom plural form. Returns null if nothing is found.
	/// </summary>
	/// <returns>The text asset.</returns>
	/// <param name="key">Key.</param>
	/// <param name="count">Count.</param>
	/// <param name="del">Del.</param>
	public TextAsset GetTextAsset(string key, int count, Func<int, int> pluralForm)
	{
		return GetTextAsset(key + "_" + pluralForm(count).ToString());
	}
	
	/// <summary>
	/// Returns an audio clip in the current language for the key with a custom plural form. Returns null if nothing is found.
	/// </summary>
	/// <returns>The audio clip.</returns>
	/// <param name="key">Key.</param>
	/// <param name="count">Count.</param>
	/// <param name="del">Del.</param>
	public AudioClip GetAudioClip(string key, int count, Func<int, int> pluralForm)
	{
		return GetAudioClip(key + "_" + pluralForm(count).ToString());
	}
	
	/// <summary>
	/// Returns a prefab in the current language for the key with a custom plural form. Returns null if nothing is found.
	/// </summary>
	/// <returns>The prefab.</returns>
	/// <param name="key">Key.</param>
	/// <param name="count">Count.</param>
	/// <param name="del">Del.</param>
	public GameObject GetPrefab(string key, int count, Func<int, int> pluralForm)
	{
		return GetPrefab(key + "_" + pluralForm(count).ToString());
	}
	
	/// <summary>
	/// Returns a texture in the current language for the key with a custom plural form. Returns null if nothing is found.
	/// </summary>
	/// <returns>The texture.</returns>
	/// <param name="key">Key.</param>
	/// <param name="count">Count.</param>
	/// <param name="del">Del.</param>
	public Texture GetTexture(string key, int count, Func<int, int> pluralForm)
	{
		return GetTexture(key + "_" + pluralForm(count).ToString());
	}
	
	/// <summary>
	/// Gets a text asset for the current language, returns null if nothing is found
	/// </summary>
	public TextAsset GetTextAsset(string key)
	{
		LocalizedObject localizedObject = GetLocalizedObject(key);
		
		if(localizedObject != null)
		{
			return Resources.Load(LanguageRuntimeData.TextAssetsFolderPath(CheckLanguageOverrideCode(localizedObject)) + "/" + key) as TextAsset;
		}
		
		if(verboseLogging)
		{
			Debug.LogError("Could not get text asset with key: " + key);
		}
		
		return null;
	}
	
	/// <summary>
	/// Gets the audio clip for the current language, returns null if nothing is found
	/// </summary>
	public AudioClip GetAudioClip(string key)
	{
		LocalizedObject localizedKey = GetLocalizedObject(key);
      
		if(localizedKey != null)
		{
			return Resources.Load(LanguageRuntimeData.AudioFilesFolderPath(CheckLanguageOverrideCode(localizedKey)) + "/" + key) as AudioClip;
		}
		
		if(verboseLogging)
		{
			Debug.LogError("Could not get audio clip with key: " + key);
		}

        return null;
	}
	/// <summary>
	/// Gets the prefab game object for the current language, returns null if nothing is found
	/// </summary>
	public GameObject GetPrefab(string key)
	{
		LocalizedObject localizedObject = GetLocalizedObject(key);
      
		if(localizedObject != null)
		{
			return Resources.Load(LanguageRuntimeData.PrefabsFolderPath(CheckLanguageOverrideCode(localizedObject)) + "/" + key) as GameObject;
		}

		if(verboseLogging)
		{
			Debug.LogError("Could not get prefab with key: " + key);
		}

        return null;
	}
		
	/// <summary>
	/// Gets a texture for the current language, returns null if nothing is found
	/// </summary>
	public Texture GetTexture(string key)
	{
		LocalizedObject localizedObject = GetLocalizedObject(key);
      
		if(localizedObject != null)
		{
			return Resources.Load(LanguageRuntimeData.TexturesFolderPath(CheckLanguageOverrideCode(localizedObject)) + "/" + key) as Texture;
		}

		if(verboseLogging)
		{
			Debug.LogError("Could not get texture with key: " + key);
		}

        return null;
	}
	
	/// <summary>
	/// Helper method that checks and gets the language override code of the object is overridden.
	/// If it's not - it will return the currently loaded language.
	/// </summary>
	string CheckLanguageOverrideCode(LocalizedObject localizedObject)
	{
		if(localizedObject == null)
		{
			return loadedLanguage;
		}
	
		string objectLanguage = localizedObject.OverrideLocalizedObject ? localizedObject.OverrideObjectLanguageCode : loadedLanguage;
		if(string.IsNullOrEmpty(objectLanguage))
		{
			objectLanguage = loadedLanguage;
		}
		
		return objectLanguage;
	}

	/// <summary>
	/// Returns whether or not the key is available in the system.
	/// </summary>
	public bool HasKey(string key)
	{
		return GetLocalizedObject(key) != null;
	}
	
	/// <summary>
	/// Gets the localized object from the localizedObjectDataBase
	/// </summary>
	private LocalizedObject GetLocalizedObject(string key)
	{
		LocalizedObject localizedObject;
		languageDatabase.TryGetValue(key, out localizedObject);
        return localizedObject;
	}

#endregion
}
}//namespace SmartLocalization