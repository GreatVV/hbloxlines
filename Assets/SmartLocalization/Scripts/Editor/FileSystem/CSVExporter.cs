// CSVExporter.cs
//
// Written by Niklas Borglund and Jakob Hillerström
//
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartLocalization.Editor
{
	/// <summary>
	/// The delimiter type for CSV
	/// </summary>
	public enum CSVDelimiter
	{
		/// <summary> COMMA</summary>
		COMMA,
		/// <summary> SEMI_COLON</summary>
		SEMI_COLON,
		/// <summary> TAB</summary>
		TAB,
		/// <summary> VERTICAL_BAR</summary>
		VERTICAL_BAR,
		/// <summary> CARET</summary>
		CARET,
	}

	/// <summary>
	/// A CSV Exporter
	/// </summary>
	public static class CSVExporter
	{
		/// <summary>
		/// Gets the actual delimiter char based on the CSVDelimiter type
		/// </summary>
		/// <param name="delimiter">The delimiter type</param>
		/// <returns>the delimiter</returns>
		public static char GetDelimiter(CSVDelimiter delimiter)
		{
			switch(delimiter)
			{
				case CSVDelimiter.COMMA:
					return ',';
				case CSVDelimiter.SEMI_COLON:
					return ';';
				case CSVDelimiter.TAB:
					return '\t';
				case CSVDelimiter.VERTICAL_BAR:
					return '|';
				case CSVDelimiter.CARET:
					return '^';
				default:
					return ',';
			}
		}

		/// <summary>
		/// Write the csv to file
		/// </summary>
		/// <param name="path">The destination path</param>
		/// <param name="delimiter">The delimiter to separate values with</param>
		/// <param name="input">The Values</param>
		public static void WriteCSV(string path, char delimiter, List<List<string>> input)
		{
			using (var sw = new StreamWriter(path))
			{
				foreach (List<string> list in input)
				{
					var sb = new StringBuilder();
					bool first = true;
					foreach (string value in list)
					{
						string replacedValue = value.Replace("\"", @"\" + "\"");

						if (!first)
						{
							sb.Append(delimiter);
						}
						first = false;
						if (replacedValue.IndexOf(delimiter) >= 0)
						{
							sb.Append('"');
							sb.Append(replacedValue);
							sb.Append('"');
						}
						else
						{
							sb.Append('"');
							sb.Append(replacedValue);
							sb.Append('"');
						}
					}
					sw.WriteLine(sb.ToString());
				}
			}
		}

		/// <summary>
		/// Write a combined CSV to File
		/// </summary>
		/// <param name="path">The destination path</param>
		/// <param name="delimiter">The delimiter to separate values with</param>
		public static void WriteCSV(string path, char delimiter, List<string> keys, Dictionary<string, Dictionary<string, string>> languages)
		{
			keys.Sort();
			using (var sw = new StreamWriter(path))
			{
				var sb = new StringBuilder();
				sb.Append("\"\"" + delimiter);
				int count = 0;
				foreach(string language in languages.Keys)
				{
					sb.Append('"' + language + '"');

					count++;
					if(count < languages.Keys.Count)
					{
						sb.Append(delimiter);
					}
				}
				sw.WriteLine(sb.ToString());

				foreach(string key in keys)
				{
					string replacedValue = key.Replace("\"", @"\" + "\"");
					sb = new StringBuilder();
					sb.Append('"' + replacedValue + '"');
					sb.Append(delimiter);

					count = 0;
					foreach(var pair in languages)
					{
						if(pair.Value.ContainsKey(key))
						{
							replacedValue = pair.Value[key].Replace("\"", @"\" + "\"");
							sb.Append('"' + replacedValue + '"');
						}
						else
						{
							sb.Append("\"\"");
						}

						if(count < languages.Count)
						{
							sb.Append(delimiter);
						}
					}

					sw.WriteLine(sb.ToString());
				}
			}
		}
		
		
		/// <summary>
		/// Read a csv file
		/// </summary>
		/// <param name="path">The path to the file</param>
		/// <param name="delimiter">The delimiter used in the file</param>
		/// <returns>The parsed csv values</returns>
		public static List<List<string>> ReadCSV(string path, char delimiter)
		{
			var result = new List<List<string>>();
			using (var sr = new StreamReader(path))
			{
				string multiline = null;
				bool inQuote = false;
				int quoteStart = -1;
				int quoteEnd = -1;
				string line;
				List<string> tmp = null;
				while ((line = sr.ReadLine()) != null) 
				{
					int itemStart = 0;

					if(!inQuote)
					{
						tmp = new List<string>();
					}

					int i = 0;
					string previousChar = string.Empty;
					for (i = 0; i < line.Length; i++)
					{
						char currentChar = line[i];
						if (currentChar == '"' && previousChar != @"\")
						{
							inQuote = !inQuote;
						}
						if (inQuote && quoteStart == -1)
						{
							if(multiline == null || multiline == string.Empty)
							{
								quoteStart = i+1;
							}
							else
							{
								quoteStart = 0;
							}
						}
						if (!inQuote && quoteStart != -1)
						{
							quoteEnd = i;
							string stringToAdd = multiline + line.Substring(quoteStart, quoteEnd - quoteStart);
							stringToAdd = stringToAdd.Replace(@"\" + "\"", "\"");
							tmp.Add(stringToAdd);
							quoteStart = -1;
							quoteEnd = -1;
							i++;
							itemStart = i + 1;
						}
						else if (!inQuote && i == line.Length - 1)
						{
							string stringToAdd = multiline + line.Substring(itemStart, i - itemStart + 1);
							stringToAdd = stringToAdd.Replace(@"\" + "\"", "\"");
							tmp.Add(stringToAdd);
						}
						else if (!inQuote && line[i] == delimiter)
						{
							string stringToAdd = multiline + line.Substring(itemStart, i - itemStart);
							stringToAdd = stringToAdd.Replace(@"\" + "\"", "\"");
							tmp.Add(stringToAdd);
							itemStart = i + 1;
						}
						previousChar = currentChar.ToString();
					}
					if(!inQuote)
					{
						result.Add(tmp);
						multiline = string.Empty;
					}
					else
					{
						if(multiline == null)
						{
							multiline = string.Empty;
						}

						if(quoteStart != -1)
						{
							multiline += line.Substring(quoteStart, i - quoteStart) + "\n";
							quoteStart = -1;
						}
						else
						{
							multiline += line + "\n";
						}
					}
				}
			}

			return result; 
		}
	}
}

